FROM ubuntu:22.04

LABEL maintainer.name="Stephen Roche"
LABEL maintainer.email="str55@pitt.edu"

ENV LANG=C.UTF-8

ARG ROOT_BIN=root_v6.26.10.Linux-ubuntu22-x86_64-gcc11.3.tar.gz

WORKDIR /opt

SHELL ["/bin/bash", "-c"]

COPY root-packages root-packages
COPY madgraph-packages madgraph-packages
COPY python-packages python-packages

RUN apt-get update -qq \
 && ln -sf /usr/share/zoneinfo/UTC /etc/localtime \
 && apt-get -y install $(cat root-packages) wget\
 && rm -rf /var/lib/apt/lists/*\
 && wget https://root.cern/download/${ROOT_BIN} \
 && tar -xzvf ${ROOT_BIN} \
 && rm -f ${ROOT_BIN} \
 && echo /opt/root/lib >> /etc/ld.so.conf \
 && ldconfig
RUN yes | unminimize

ENV ROOTSYS /opt/root
ENV PATH $ROOTSYS/bin:$PATH
ENV PYTHONPATH $ROOTSYS/lib:$PYTHONPATH
ENV CLING_STANDARD_PCH none

RUN pip3 install $(cat python-packages)

RUN wget https://launchpad.net/mg5amcnlo/3.0/3.3.x/+download/MG5_aMC_v3.3.2.tar.gz \
 && tar -zxf MG5_aMC_v3.3.2.tar.gz \
 && rm MG5_aMC_v3.3.2.tar.gz \
 && cd MG5_aMC_v3_3_2 \
 && ./bin/mg5_aMC -f ../madgraph-packages

ENV PYTHIA8 /opt/MG5_aMC_v3_3_2/HEPTools/pythia8
ENV PYTHIA8DATA /opt/MG5_aMC_v3_3_2/HEPTools/pythia8/share/Pythia8/xmldoc

RUN git clone https://github.com/delphes/delphes.git \
 && cd delphes \
 && make HAS_PYTHIA8=true

ENV PYTHONPATH /opt/delphes/python:$PYTHONPATH
ENV LD_LIBRARY_PATH /opt/delphes:$LD_LIBRARY_PATH
ENV LD_LIBRARY_PATH /opt/delphes/external:$LD_LIBRARY_PATH
ENV ROOT_INCLUDE_PATH /opt/delphes/external:$ROOT_INCLUDE_PATH
ENV PYTHONPATH $PYTHONPATH:$ROOT_INCLUDE_PATH
ENV LD_LIBRARY_PATH /opt/MG5_aMC_v3_3_2/HEPTools/lhapdf6_py3/lib:$LD_LIBRARY_PATH
ENV LD_LIBRARY_PATH /opt/MG5_aMC_v3_3_2/HEPTools/pythia8/lib:$LD_LIBRARY_PATH
