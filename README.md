# Particle Physics Simulation Tool Docker Image
## DockerHub Link
[Ubuntu 22.04, ROOT 6.26.06, Delphes3, Pythia8, MadGraph 3.3.2](https://hub.docker.com/repository/docker/stroche/particle-physics-simulation)

## Use
```bash
docker run -it stroche/particle-physics-simulation
```

## Tools included
- ROOT
- MadGraph
- Pythia
- Delphes